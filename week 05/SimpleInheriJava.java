import java.util.Scanner;
class Animal {
    Animal() {
        System.out.println("Animal barks");
    }
}

class Dog extends Animal {
    Dog() {
        System.out.println("Bow Bow");
    }
}
class Main{
    public static void main(String[] args) {
        Dog obj = new Dog();
    }
}
