#include<iostream>
using namespace std;
class Parent{
    public:
    Parent()
    {
        cout<<"I'm parent\n";
    }
};
class Child1 : public Parent{
    public:
    Child1(){
        cout<<"parent to child1\n";
    }
};
class Child2 : public Parent{
    public:
    Child2(){
        cout<<"parent to child2\n";
    }
};
class Grandchild : public Child1,public Child2{
    public:
    Grandchild(){
        cout<<"from child1,2 to grandchild\n";
    }
};
int main(){
    Grandchild obj;
    return 0;
}
