import java.util.*;
class Parent1
{
    Parent1()
    {
        System.out.println("This is Parent1");
    }
}
class Child1 extends Parent1
{
    Child1()
    {
        System.out.println("Parent1 to Child1");
    }
}
class Parent2
{
    Parent2()
    {
        System.out.println("This is Parent2");
    }
}
class Child2 extends Parent2
{
    Child2()
    {
        System.out.println("Parent2 to Child2");
    }
}
class Child3 extends Parent2
{
    Child3()
    {
        System.out.println("Parent2 to Child3");
    }
}
class Grandchild extends Child1
{
    Grandchild()
    {
        System.out.println("Child1 to Grandchild");
    }
}
interface Parent3
{
    public void Parent3();
}
interface Parent4
{
    public void Parent4();
}
class Child4 implements Parent3,Parent4
{
    public void Parent3()
    {
        System.out.println("This is a Parent3");
    }
    public void Parent4()
    {
        System.out.println("This is a Parent4");
    }
    Child4()
    {
        System.out.println("Parent3,4 to Child4");
    }
}
class Main
{
    public static void main(String[] args)
    {
        System.out.println("Single Inheritance :");
        Child1 obj1=new Child1();
        System.out.println("Hierarchical Inheritance :");
        Child2 obj2=new Child2();
        Child3 obj3=new Child3();
        System.out.println("MultiLevel Inheritance :");
        Grandchild obj4=new Grandchild();
        System.out.println("Multiple Inheritance :");
        Child4 obj5=new Child4();
        obj5.Parent3();
        obj5.Parent4();
    }
}
