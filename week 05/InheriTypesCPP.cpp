#include<iostream>
using namespace std;
class Parent1 {
public:
    Parent1(){
    cout<<"parent1 !"<<endl;
    }
};
class Parent2 {
public :
    Parent2(){
    cout<<"parent2 !"<<endl;
    }
};
// SIMPLE INHERITENCE
class Child1 : public Parent1{
    public :
    Child1()
    {
        cout<<"Parent1 to Child1 !"<<endl;
    }
};
// MULTIPLE INHERITENCE
class Child2 : public Parent1 , public Parent2 {
    public :
    Child2()
    {
        cout<<"Parent1,2  to Child2 !"<<endl;
    }
};
// MULTILEVEL INHERITENCE
class Child3 : public Child1{
    public:
    Child3()
    {
        cout<<"child1 to Child3 !"<<endl;
    }
};
//HIERARCHIAL INHERITENCE
class Child4 : public Parent2{
    public :
    Child4()
    {
        cout<<"Parent2 to Child4 !"<<endl;
    }
};
class Child5 : public Parent2{
    public :
    Child5()
    {
        cout<<"Parent2 to Child5 !"<<endl;
    }
};
int main()
{
    cout<<"Simple Inheritence "<<endl;
    Child1 objc1;
    cout<<"\nMultiple Inheritence "<<endl;
    Child2 objc2;
    cout<<"\nMultilevel Inheritence "<<endl;
    Child3 objc3;
    cout<<"\nHierarchial Inheritence"<<endl;
    Child4 objc4;
    Child5 objc5;
}
