import java.util.*;
class Parent
{
    public void displayMsg();
}
class Child1 extends Parent
{
    default void displayMsg()
    {
        System.out.println("This is Child1");
    }
}
class Child2 extends Parent
{
    default void displayMsg()
    {
        System.out.println("This is Child2");
    }
}
class Grandchild extends Child1,Child2 
{
    public void displayMsg()
    {
        System.out.println("This is Grandchild");
    }
}
class Main
{
    public  static void main(String[] args)
    {
        Grandchild obj=new Grandchild();
        obj.displayMsg();
    }
}

