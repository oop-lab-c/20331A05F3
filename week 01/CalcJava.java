import java.util.*;
class Arithmetic
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        System.out.println("ENTER TWO INTEGERS : ");
        int a = input.nextInt();
        int b = input.nextInt();
        System.out.println("Enter the operator : ");
        char op = input.next().charAt(0);
        if (op == '+')
            System.out.println("sum = " + (a + b));
        else if (op == '-')
            System.out.println("difference = : " + (a - b));
        else if (op == '*')
            System.out.println("product = " + (a * b));
        else if (op == '/') {
            if (b != 0)
                System.out.println("Quotient = " + (a / b));
            else
                System.out.println("DIVISION IS NOT POSSIBLE ");
        }
        else if (op == '%') 
                System.out.println(" Remainder = " + (a % b));
        else
            System.out.println("Enter valid operator");
    }
}
