import java.util.*;

class Checking
 {
   void Check(int a){
     if(a%2==0)
      System.out.println(a+" is Even number");
     else
      System.out.println(a+" is Odd number");
   }
}

public class Evenodd
 {
   public static void main(String args[])
   { 
    System.out.println("Enter a number");
    Scanner input = new Scanner(System.in);
    int a = input.nextInt();
    Checking obj = new Checking(); 
    obj.Check(a);   
   }
 }

