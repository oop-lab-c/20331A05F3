import java.util.*;
class Sync{    
  synchronized static void display(int n)
  {  
   for(int i=1;i<=5;i++)
   {  
     System.out.println(n+"*"+i+"="+n*i); 
   } 
 }
}
class Thread1 extends Thread
{  
    public void run()
    {  
        Sync.display(2);  
    }  
}
class Thread2 extends Thread
{  
    public void run()
    { 
        Sync.display(3);  
    }  
}  
   
class Main{  
public static void main(String[] args){
        Thread1 obj1=new Thread1();  
        Thread2 obj2=new Thread2(); 
        obj1.start();  
        obj2.start();  
    }  
}  
