abstract class  Animal{
    abstract void bark();
    void walk(){
        System.out.println("walk forward");
    }
}
class Dog extends Animal{
    void bark(){
         System.out.println("Bow Bow");
    }
    
}
class Main{
    public static void main(String[] args){
        Dog obj=new Dog();
        obj.bark();
        obj.walk();
    }
}
