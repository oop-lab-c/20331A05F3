#include<iostream>
using namespace std;
class Animal{
    public:
    virtual void bark()=0;
};
class Dog:public Animal{
    public:
    void bark(){
        cout<<"Bow Bow"<<endl;
    }
};
int main(){
    Dog obj;
    obj.bark();
    return 0;
}
