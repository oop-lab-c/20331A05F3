class Overload{
    void display(int a){
        System.out.println(a);
    }
    void display(int a, int b){
        System.out.println(a+b);
    }
}
class Main{
    public static void main(String[] args){
        Overload obj=new Overload();
        obj.display(2);
        obj.display(2,3);
    }
}
