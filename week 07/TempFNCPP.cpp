#include<iostream>
using namespace std;
template<typename type>

type add(type a,type b)
{
    return a+b;
}

template<class type>
class Demo1
{
    type a;
    public:
    Demo1(type x)
    {
        a=x;
    }
    type display()
    {
        return a;
    }
};

int main()
{
    int i1,i2;
    float f1,f2;
    cout<<"Enter two integers :"<<endl;
    cin>>i1>>i2;
    cout<<"Enter two floating point numbers :"<<endl;
    cin>>f1>>f2;
    cout<<"Demonstration of template functions "<<endl;
    cout<<add<float>(f1,f2)<<endl;
    cout<<add<int>(i1,i2)<<endl;
    cout<<"\nDemonstration of template class "<<endl;
    Demo1<int>obj1(i1);
    Demo1<float>obj2(f1);
    cout<<"Integer entered is : "<<obj1.display()<<endl;
    cout<<"Floating point number entered is : "<<obj2.display()<<endl;
    return 0;
}
