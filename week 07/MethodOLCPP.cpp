#include<iostream>
using namespace std;
class Overload{
    public:
    void func(int a)
    {
        cout<<a<<endl;
    }
    void func(int a,int b)
    {
        cout<<a+b<<endl;
    }
};
int main(){
    Overload obj;
    obj.func(5);
    obj.func(10,5);
    return 0;
}
