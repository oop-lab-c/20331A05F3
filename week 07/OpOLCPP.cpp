#include<iostream>
using namespace std;
class Overload {
 public:
	int real, imag;
	Overload(int r = 0, int i = 0) {
        real = r; imag = i;
        }
	Overload operator + (Overload const &obj) {
		Overload res;
		res.real = real + obj.real;
		res.imag = imag + obj.imag;
		return res;
	}
	void print() {
         cout << real << " + i" << imag << '\n'; 
         }
};

int main()
{
	Overload c1(1, 1), c2(2, 2);
	Overload c3 = c1 + c2;
	c3.print();
}
