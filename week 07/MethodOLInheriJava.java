class Overload{
    void display(){
       System.out.println("Hello");
   }
}
class Method extends Overload{
   void display(String name){
       System.out.println("Hello "+name);
   }
}
class Main{
   public static void main(String[] args){
       Method obj=new Method();
       obj.display();
       obj.display("Harsha");
   }
}
