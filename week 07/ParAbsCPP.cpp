#include<iostream>
using namespace std;
class Animal{
    public:
      virtual void walk(){
           cout<<"Walk Forward"<<endl;
      } 
     virtual void bark(){
        
    }
};
class Dog:public Animal{
    public:
    void bark(){
        cout<<"Bow Bow"<<endl;
    }
};
int main(){
    Dog obj;
    obj.bark();
    obj.walk();
    return 0;
}
