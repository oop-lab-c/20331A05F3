class Override{
    public void display(int a, int b){
        System.out.println(a-b);
    }
}
class Method extends Override{
    public void display(int a, int b){
        System.out.println(a+b);
    }
}
class Main{
    public static void main(String[] args){
        Method obj=new Method();
        obj.display(2,2);
    }
}
