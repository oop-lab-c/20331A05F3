interface Animal{
    void bark();
    void walk();
}
class Dog implements Animal{
    public void bark(){
        System.out.println("Bow Bow");
    }
    public void walk(){
        System.out.println("walk forward");
    }
}
class Main{   
    public static void main(String[] args){
        Dog obj=new Dog();
        obj.bark();
        obj.walk();
    }
}
