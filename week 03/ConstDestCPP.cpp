#include<iostream>
using namespace std;
class Student{
    public:
        string fullname;
        int rollno;
        double sempercentage;
        string collegename;
        int collegecode;
    Student(string a,int b,double c, string d,int e){
         fullname=a;
         rollno=b;
         sempercentage=c;
         collegename=d;
         collegecode=e;
    }
    void displaymsg(){
        cout<<fullname<<" "<<rollno<<" "<<sempercentage<<" "<<collegename<<" "<<collegecode<<endl;
    }
    ~Student(){
        cout<<"Destructor Invoked"<<endl;
    }
};
int main(){
    Student obj("Harsha",07,87.4,"MVGR",33);
    obj.displaymsg();
}
