import java.util.*;
class AgeException extends Exception{
    AgeException(String msg){
        System.out.println(msg);
    }
}
class Main {
        public static void main(String[] args) {
        Scanner obj=new Scanner(System.in); 
        System.out.print("Enter user age :"); 
        try{
            int age=obj.nextInt();
            Vote(age);
        }catch(Exception e){
            System.out.println(e);
        }finally{
            System.out.println("Try catch block executed");
        }
        }
        public static void Vote(int a) throws AgeException{
        if(a<18){
            throw new AgeException("Not eligible for voting");
        }
        else
        System.out.println("Eligible for voting");
        }
       
        
    }  
