#include<iostream>
#include<iomanip>
using namespace std;
void fun(){
    cout<<"endl use"<<endl;
    cout<<"line1"<<endl<<"line2"<<endl;
    cout<<"ends use"<<endl;
    cout<<'a'<<ends;
    cout<<'b'<<ends;
    cout<<'c'<<endl;
    cout<<"flush use"<<endl;
    cout<<"example of flush"<<flush<<endl;
    cout<<"setw use"<<endl;
    cout<<setw(5)<<10<<endl;
    cout<<setw(5)<<5<<endl;
    cout<<"setfill use"<<endl;
    cout<<setfill('$')<<setw(5)<<12<<endl;
    cout<<setfill('#')<<setw(5)<<1<<endl;
    cout<<"setpresition use"<<endl;
    cout<<setprecision(5)<<0.123456789<<endl;
    cout<<setprecision(7)<<0.123456789<<endl;
}
int main(){
    fun();
}
