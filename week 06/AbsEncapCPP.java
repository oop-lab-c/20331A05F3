import java.util.*;

class AccessSpecifierDemo {
    private int priVar = 1;
    public int pubVar = 2;
    protected int proVar = 3;
    int priValue, proValue, pubValue;

    public void setVar() {
        priValue = priVar;
        pubValue = pubVar;
        proValue = proVar;
    }

    public void getVar() {
        System.out.println(priValue);
        System.out.println(pubValue);
        System.out.println(proValue);
    }

    public static void main(String[] args) {
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar();
        obj.getVar();
    }
}